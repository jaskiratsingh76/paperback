# README #

A (very) minimalist book searching application.  

Search your favorite books, quotes, and even characters from the novels.  
Integrated with an ISBN scanner.  

A WP8 book searching app written in C# and XAML.  

[http://www.windowsphone.com/en-in/store/app/paperback/b61ba1d3-e732-4efb-9187-e8a409506c8a](http://www.windowsphone.com/en-in/store/app/paperback/b61ba1d3-e732-4efb-9187-e8a409506c8a)

