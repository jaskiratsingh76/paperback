﻿#pragma checksum "E:\WP8 Apps\Paperback\Paperback\Pages\BookDetails.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4F3788BF3AFD0FE8C7658C893E132A13"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace Paperback.Pages {
    
    
    public partial class BookDetails : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.TextBlock tbBookTitle;
        
        internal System.Windows.Controls.Image imgCoverImage;
        
        internal System.Windows.Controls.TextBlock tbAuthor;
        
        internal Microsoft.Phone.Controls.Rating ratingControl;
        
        internal System.Windows.Controls.TextBlock tbYearAndPages;
        
        internal System.Windows.Controls.TextBlock tbPublisher;
        
        internal System.Windows.Controls.TextBlock tbIndustryIdentifierType;
        
        internal System.Windows.Controls.TextBlock tbIndustryIdentifier;
        
        internal System.Windows.Controls.TextBlock tbTitleDesc;
        
        internal System.Windows.Controls.TextBlock tbNoDescriptionFound;
        
        internal System.Windows.Controls.TextBlock tbDescription;
        
        internal Microsoft.Phone.Controls.PivotItem pivotReviews;
        
        internal System.Windows.Controls.TextBlock tbTitleReviews;
        
        internal System.Windows.Controls.TextBlock tbNoReviewsFound;
        
        internal System.Windows.Controls.ProgressBar pbReviews;
        
        internal Microsoft.Phone.Controls.LongListSelector llsReviews;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/Paperback;component/Pages/BookDetails.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.tbBookTitle = ((System.Windows.Controls.TextBlock)(this.FindName("tbBookTitle")));
            this.imgCoverImage = ((System.Windows.Controls.Image)(this.FindName("imgCoverImage")));
            this.tbAuthor = ((System.Windows.Controls.TextBlock)(this.FindName("tbAuthor")));
            this.ratingControl = ((Microsoft.Phone.Controls.Rating)(this.FindName("ratingControl")));
            this.tbYearAndPages = ((System.Windows.Controls.TextBlock)(this.FindName("tbYearAndPages")));
            this.tbPublisher = ((System.Windows.Controls.TextBlock)(this.FindName("tbPublisher")));
            this.tbIndustryIdentifierType = ((System.Windows.Controls.TextBlock)(this.FindName("tbIndustryIdentifierType")));
            this.tbIndustryIdentifier = ((System.Windows.Controls.TextBlock)(this.FindName("tbIndustryIdentifier")));
            this.tbTitleDesc = ((System.Windows.Controls.TextBlock)(this.FindName("tbTitleDesc")));
            this.tbNoDescriptionFound = ((System.Windows.Controls.TextBlock)(this.FindName("tbNoDescriptionFound")));
            this.tbDescription = ((System.Windows.Controls.TextBlock)(this.FindName("tbDescription")));
            this.pivotReviews = ((Microsoft.Phone.Controls.PivotItem)(this.FindName("pivotReviews")));
            this.tbTitleReviews = ((System.Windows.Controls.TextBlock)(this.FindName("tbTitleReviews")));
            this.tbNoReviewsFound = ((System.Windows.Controls.TextBlock)(this.FindName("tbNoReviewsFound")));
            this.pbReviews = ((System.Windows.Controls.ProgressBar)(this.FindName("pbReviews")));
            this.llsReviews = ((Microsoft.Phone.Controls.LongListSelector)(this.FindName("llsReviews")));
        }
    }
}

