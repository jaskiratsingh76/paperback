﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace Paperback.Pages
{
    public partial class SearchPage : PhoneApplicationPage
    {
        public SearchPage()
        {
            InitializeComponent();
            
            // attach event handler to detect search key
            tbSearchInput.KeyDown += tbSearchInput_KeyDown;
        }

        void tbSearchInput_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            // detect enter key and begin search
            if (e.Key == Key.Enter)
            {
                // begin search
                InitSearch();
                return;
            }
        }

        private void InitSearch()
        {
            string origString;
            string safeString;
            // fetch the search textbox value and remove exccess spaces
            origString = tbSearchInput.Text.Trim();
            
            // remove excessive spaces and unsafe characters
            string unsafeChars = "[^a-zA-Z0-9 ]";
            safeString = Regex.Replace(origString, unsafeChars, "").Trim();

            // return if string is empty spaces
            if (safeString.Length == 0)
                return;

            // replace spaces with +
            safeString = safeString.Replace(" ", "+");

            // navigate to search results page, passing the 
            // query string as a parameter
            Uri pageId = new Uri("/Pages/SearchResults.xaml?SOURCE=SEARCH&SAFE=" + safeString + "&ORIG=" + origString, UriKind.Relative);
            NavigationService.Navigate(pageId);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // set focus to search box
            tbSearchInput.UpdateLayout();
            tbSearchInput.Focus();
        }
    }
}