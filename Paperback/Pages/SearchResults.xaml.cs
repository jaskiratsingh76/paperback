﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Paperback.ViewModels;
using System.Threading.Tasks;
using System.Collections.ObjectModel;


namespace Paperback.Pages
{
    public partial class SearchResults : PhoneApplicationPage
    {
        public static List<GoogleBook> books { get; set; }
        public static ObservableCollection<GoogleBook> obvs_books { get; set; }
        public bool FetchBooks { get; set; }
        public int MaxIndex { get; set; }
        public bool MoreBooksExist { get; set; }
        public string SearchString { get; set; }

        public SearchResults()
        {
            InitializeComponent();
            // bind event handler
            llsResults.SelectionChanged += llsResults_SelectionChanged;
            btnLoadMoreBooks.Click += btnLoadMoreBooks_Click;
            // search is to be done
            this.MoreBooksExist = true;
            FetchBooks = true;
        }

        void btnLoadMoreBooks_Click(object sender, RoutedEventArgs e)
        {
            // load more books and update the item source 
            // of the long list selector
            if (this.MoreBooksExist)
            {
                // fetch more items for the long list selector
                UpdateBooksList(this.SearchString);
            }
        }

        private void ShowProgressBar()
        {
            if (SystemTray.ProgressIndicator == null)
            {
                SystemTray.ProgressIndicator = new ProgressIndicator();
                SystemTray.ProgressIndicator.Text = "Fetching books...";    
                SystemTray.ProgressIndicator.IsIndeterminate = true;
            }
            SystemTray.ProgressIndicator.IsVisible = true;
        }

        private void HideProgressBar()
        {
            if (SystemTray.ProgressIndicator != null)
            {
                SystemTray.ProgressIndicator.IsVisible = false;
            }
        }

        private async void PerformSearch(string origString, string safeString)
        {
            // show overlay progress bar
            pbFetchingBooks.Visibility = System.Windows.Visibility.Visible;
            
            // add "RESULTS FOR"
            origString = "RESULTS FOR " + origString;
            int total_items = 0;

            // limit to 45 characters and capitalize
            if (origString.Length > 45)
            {
                origString = origString.Substring(0, 45);
                origString += "...";
            }
            origString = origString.ToUpper();
            
            // bind to page
            tbSearchQuery.Text = origString;

            // perform search and fetch books
            books = await Crawler.GetBooks(safeString, 0);
            
            // hide overlay progress bar
            pbFetchingBooks.Visibility = Visibility.Collapsed;

            if (books == null)
            {
                // set to no result found
                llsResults.Visibility = Visibility.Collapsed;
                tbNoResultFound.Visibility = Visibility.Visible;
                tbInternetFailure.Visibility = Visibility.Collapsed;
            }
            else
            {
                llsResults.Visibility = Visibility.Visible;
                tbNoResultFound.Visibility = Visibility.Collapsed;
                tbInternetFailure.Visibility = Visibility.Collapsed;
                
                // populate the long list selector 
                obvs_books = new ObservableCollection<GoogleBook>(books);
                //llsResults.ItemsSource = books;
                llsResults.ItemsSource = obvs_books;

                // find the maximum index of books returned
                total_items = books.Count;
                if(total_items < 10)
                {
                    // no more books exist
                    this.MoreBooksExist = false;
                }
            }
            
            // fetching operation complete
            FetchBooks = false;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            string requestSource;
            string safeString = "";
            string origString = "";
            
            // check if search is to be done
            if (FetchBooks == true)
            {
                // check request source page name
                try
                {
                    // extract source value
                    NavigationContext.QueryString.TryGetValue("SOURCE", out requestSource);

                    // check source value against the sources
                    if (requestSource.Equals("SCANNER"))
                    {
                        // request came from ISBN scanner
                        // extract the passed ISBN
                        NavigationContext.QueryString.TryGetValue("ISBN", out safeString);
                        origString = safeString;
                    }
                    else if (requestSource.Equals("SEARCH"))
                    {
                        // request came from search page
                        // extract the passed query string
                        NavigationContext.QueryString.TryGetValue("SAFE", out safeString);
                        NavigationContext.QueryString.TryGetValue("ORIG", out origString);
                    }
                    this.SearchString = safeString;
                }
                catch (Exception ex)
                {
                    MainPage.console(ex.StackTrace + ", MSG: " + ex.Message);
                }
                
                // perform search
                PerformSearch(origString, safeString);
            }
        }

        //update the list with more items
        private async void UpdateBooksList(string safeString)
        {
            // show progress bar
            ShowProgressBar();

            // find max index of books already loaded
            int count = obvs_books.Count;
            GoogleBook g = obvs_books[count - 1];
            this.MaxIndex = g.Index;
            
            // get the books
            List<GoogleBook> more_books = new List<GoogleBook>();
            more_books = await Crawler.GetBooks(safeString, this.MaxIndex + 1);
            if (more_books != null)
            {
                //books.AddRange(more_books);
                foreach (GoogleBook b in more_books)
                {
                    obvs_books.Add(b);
                }

                // hide progress bar
                HideProgressBar();
            }
        }

        // show details of the selected book
        void llsResults_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GoogleBook book = llsResults.SelectedItem as GoogleBook;
            if (book != null)
            {
                // construct page uri
                Uri pageId = new Uri("/Pages/BookDetails.xaml?INDEX=" + book.Index.ToString(), UriKind.Relative);

                // reset selection
                llsResults.SelectedItem = null;

                // navigate to page
                NavigationService.Navigate(pageId);
            }
        }
    }
}