﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

namespace Paperback.Pages
{
    public partial class about : PhoneApplicationPage
    {
        public about()
        {
            InitializeComponent();

            tbBugsEmail.Tap += tbBugsEmail_Tap;
        }

        void tbBugsEmail_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            // send email to the developer
            string toAddress = "paperback@live.in";
            var task = new EmailComposeTask {To = toAddress};
            task.Show();
        }
    }
}