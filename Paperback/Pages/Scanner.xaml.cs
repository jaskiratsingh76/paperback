﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Devices;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using ZXing;

namespace Paperback.Pages
{
    public partial class Scanner : PhoneApplicationPage
    {
        // Constructor
        public Scanner()
        {
            InitializeComponent();
            this._AFComplete = true;
        }

        // the variables
        private PhotoCamera _phoneCamera;
        private IBarcodeReader _barcodeReader;
        private DispatcherTimer _scanTimer;
        private DispatcherTimer _AFTimer;
        private WriteableBitmap _previewBuffer;

        // autofocus property
        private bool _AFComplete { get; set; }

        // init camera when page navigated to
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            /*
             * REMOVE THIS CODE BEFORE DEPLOYING
             */
            //string scannedISBN = "9780099554547";
            //Uri pageId = new Uri("/Pages/SearchResults.xaml?SOURCE=SCANNER&ISBN=" + scannedISBN, UriKind.Relative);
            //NavigationService.Navigate(pageId);
            //return;

            // initialise camera
            _phoneCamera = new PhotoCamera(CameraType.Primary);

            // attach event handlers
            _phoneCamera.Initialized += cam_Initialised;
            // autofocus binder
            _phoneCamera.AutoFocusCompleted += _phoneCamera_AFCompleted;

            // set viewfinder source
            viewfinderBrush.SetSource(_phoneCamera);

            // init timer to scan for barcode every 250ms
            _scanTimer = new DispatcherTimer();
            _scanTimer.Interval = TimeSpan.FromMilliseconds(250);
            // scan barcode when time elapses
            _scanTimer.Tick += ScanBarcode;

            // autofocus timer
            _AFTimer = new DispatcherTimer();
            _AFTimer.Interval = TimeSpan.FromMilliseconds(1000);
            // do autofocus at each tick
            _AFTimer.Tick += DoAutoFocus;
        }

        private void DoAutoFocus(object sender, EventArgs e)
        {
            if (this._AFComplete == true)
            {
                // check if autofocus is supported
                if (_phoneCamera.IsFocusSupported == true)
                {
                    try
                    {
                        // focus camera on barcode
                        this._AFComplete = false;
                        _phoneCamera.Focus();
                    }
                    catch (Exception ex)
                    {
                        console(ex.StackTrace + ", MSG: " + ex.Message);
                        this._AFComplete = true;
                    }
                }
            }
        }

        private void ScanBarcode(object sender, EventArgs e)
        {
            _phoneCamera.GetPreviewBufferArgb32(_previewBuffer.Pixels);
            _previewBuffer.Invalidate();

            //scan the captured snapshot for barcodes
            //if a barcode is found, the ResultFound event will fire
            _barcodeReader.Decode(_previewBuffer);  
        }

        private void _phoneCamera_AFCompleted(object sender, CameraOperationCompletedEventArgs e)
        {
            this._AFComplete = true;
        }

        private void cam_Initialised(object sender, CameraOperationCompletedEventArgs e)
        {
            if (e.Succeeded)
            {
                // camera successfully initialised
                // execute the delegate asynchronously
                this.Dispatcher.BeginInvoke(delegate()
                {
                    _phoneCamera.FlashMode = FlashMode.Off;
                    _previewBuffer = new WriteableBitmap((int)_phoneCamera.PreviewResolution.Width, (int)_phoneCamera.PreviewResolution.Height);

                    // init barcode reader
                    _barcodeReader = new BarcodeReader();

                    // set supported barcode types
                    var supportedBarcodes = new List<BarcodeFormat>();
                    supportedBarcodes.Add(BarcodeFormat.EAN_13);
                    _barcodeReader.Options.PossibleFormats = supportedBarcodes;

                    // deep scanning
                    _barcodeReader.Options.TryHarder = true;

                    // attach handler if result found
                    _barcodeReader.ResultFound += _barcodeReader_ResultFound;

                    // start timer to begin scanning barcodes
                    _scanTimer.Start();

                    // start autofocus timer
                    _AFTimer.Start();
                });
            }
            else
            {
                // camera init failed
                Dispatcher.BeginInvoke(delegate()
                {
                    MessageBox.Show("Unable to initialise camera.");
                });
            }
        }

        private void _barcodeReader_ResultFound(Result scanResult)
        {
            // barcode found
            // vibrate device and update the UI
            string scannedISBN = scanResult.Text;
            VibrateController.Default.Start(TimeSpan.FromMilliseconds(100));
            tbISBN.Text = scannedISBN;
            //tbBarcodeType.Text = scanResult.BarcodeFormat.ToString();
            
            if(scannedISBN.Length >= 10)
            {
                Uri pageId = new Uri("/Pages/SearchResults.xaml?SOURCE=SCANNER&ISBN=" + scannedISBN, UriKind.Relative);
                NavigationService.Navigate(pageId);
            }

            // navigate to search page and pass ISBN
            //Uri pageId = new Uri("/Pages/SearchResults.xaml?SOURCE=SCANNER&ISBN=" + scannedISBN, UriKind.Relative);
            //NavigationService.Navigate(pageId);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            //console("Navigating away from scanner. Destroying elements");
            //return;

            // stop timers
            _scanTimer.Stop();
            _AFTimer.Stop();

            if (_phoneCamera != null)
            {
                // free camera resources and remove event handlers
                _phoneCamera.Dispose();

                // remove event handlers
                _phoneCamera.Initialized -= cam_Initialised;
                _phoneCamera.AutoFocusCompleted -= _phoneCamera_AFCompleted;
                _scanTimer.Tick -= ScanBarcode;
                _AFTimer.Tick -= DoAutoFocus;
            }
        }

        // debug console
        private void console(string message)
        {
            //System.Diagnostics.Debug.WriteLine(message);
        }
    }
}