﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Paperback.ViewModels;
using Microsoft.Phone.Tasks;

namespace Paperback.Pages
{
    public partial class BookDetails : PhoneApplicationPage
    {
        public GoogleBook SelectedBook { get; set; }
        public BookReview Reviews { get; set; }
        public PBook ChosenBook { get; set; }
        public bool IsBookSet { get; set; }

        public BookDetails()
        {
            InitializeComponent();
            ChosenBook = new PBook();
            IsBookSet = false;

            // bind chosen item selector
            llsReviews.SelectionChanged += llsReviews_SelectionChanged;
        }

        void llsReviews_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Review chosenReview = llsReviews.SelectedItem as Review;
            if (chosenReview != null)
            {
                // extract the review url 
                Uri reviewUri = new Uri(chosenReview.Link, UriKind.Absolute);

                // reset selection
                llsReviews.SelectedItem = null;

                // open the review url in web browser
                WebBrowserTask webBrowserTask = new WebBrowserTask();
                webBrowserTask.Uri = reviewUri;
                webBrowserTask.Show();
            }
        }

        private void ShowProgressBar()
        {
            if (SystemTray.ProgressIndicator == null)
            {
                SystemTray.ProgressIndicator = new ProgressIndicator();
                SystemTray.ProgressIndicator.Text = "Fetching reviews...";
                SystemTray.ProgressIndicator.IsIndeterminate = true;
            }
            SystemTray.ProgressIndicator.IsVisible = true;

            // show reviews progress bar
            pbReviews.Visibility = System.Windows.Visibility.Visible;
        }

        private void HideProgressBar()
        {
            if (SystemTray.ProgressIndicator != null)
            {
                SystemTray.ProgressIndicator.IsVisible = false;
            }

            // hide reviews progress bar
            pbReviews.Visibility = System.Windows.Visibility.Collapsed;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            int index;
            // extract title of passed book
            if (IsBookSet)
                return;
            try
            {
                // extract index of book
                index = Int32.Parse(NavigationContext.QueryString["INDEX"]);
                
                // set SelectedBook if not set
                if (IsBookSet == false)
                {
                    // find the chosen book from the search results list
                    foreach (GoogleBook book in SearchResults.obvs_books)
                    {
                        if (book.Index == index)
                        {
                            this.ChosenBook.GBook = book;
                            //this.SelectedBook = book;
                            break;
                        }
                    }

                    // check if description available
                    if (this.ChosenBook.GBook.Description == null)
                    {
                        tbNoDescriptionFound.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        tbNoDescriptionFound.Visibility = Visibility.Collapsed;
                    }

                    // set data context
                    DataContext = this.ChosenBook;
                    IsBookSet = true;

                    // fetch review for the book
                    FetchReview();
                }
            }
            catch (System.NullReferenceException ex)
            {
                // do nothing, again, bitch.
                // or rather shout out loud.
                MainPage.console(ex.StackTrace + ", MSG: " + ex.Message);
            }
        }

        private async void FetchReview()
        {
            ShowProgressBar();
            string bookTitle = this.ChosenBook.GBook.Title;
            string bookAuthor = this.ChosenBook.GBook.AuthorsList[0];
            int startIndex = 0;
            // limit to 40 characters
            if (bookTitle.Length > 40)
            {
                bookTitle = bookTitle.Substring(0, 40);
            }
            
            this.ChosenBook.ReviewOfBook = await Crawler.GetBookReviews(bookTitle, bookAuthor, startIndex);
            
            if (this.ChosenBook.ReviewOfBook == null)
            {
                llsReviews.Visibility = System.Windows.Visibility.Collapsed;
                tbNoReviewsFound.Visibility = System.Windows.Visibility.Visible;
                HideProgressBar();
            }
            else
            {
                llsReviews.Visibility = System.Windows.Visibility.Visible;
                tbNoReviewsFound.Visibility = System.Windows.Visibility.Collapsed;
                llsReviews.ItemsSource = this.ChosenBook.ReviewOfBook.Reviews;
                HideProgressBar();
            }
            
        }

    }
}