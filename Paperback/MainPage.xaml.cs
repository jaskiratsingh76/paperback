﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Paperback.Resources;
using Coding4Fun.Toolkit.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Net.NetworkInformation;

namespace Paperback
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

            // bind the LongListSelector item selected event
            //llsHomePagePivot.SelectionChanged += llsHomePagePivot_SelectionChanged;
            btnGoToSearch.Tap += btnGoToSearch_Tap;
            btnGoToScan.Tap += btnGoToScan_Tap;
            btnGoToAboutPage.Tap += btnGoToAboutPage_Tap;
            btnGoToHelpPage.Tap += btnGoToHelpPage_Tap;

        }

        void btnGoToHelpPage_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri pageId = new Uri("/Pages/Help.xaml", UriKind.Relative);
            NavigationService.Navigate(pageId);
        }

        void btnGoToAboutPage_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Uri pageId = new Uri("/Pages/About.xaml", UriKind.Relative);
            NavigationService.Navigate(pageId);
        }

        void btnGoToScan_Tap(object sender, RoutedEventArgs e)
        {
            // navigate to search page for scanning ISBN's
            Uri pageId = new Uri("/Pages/Scanner.xaml", UriKind.Relative);
            NavigationService.Navigate(pageId);
        }

        void btnGoToSearch_Tap(object sender, RoutedEventArgs e)
        {
            // check internet
            bool isNetwork = NetworkInterface.GetIsNetworkAvailable();
            if (!isNetwork)
            {
                MainPage.ShowToast("unable to connect");
                return;
            }
            // navigate to search results page for testing
            Uri pageId = new Uri("/Pages/SearchPage.xaml", UriKind.Relative);
            //Uri pageId = new Uri("/Pages/BookDetails.xaml", UriKind.Relative);
            NavigationService.Navigate(pageId);
        }

        void llsHomePagePivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LongListSelector selector = sender as LongListSelector;
            var page = selector.SelectedItem;
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/Images/pb_info.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // bind the click event handler
        //    appBarButton.Click += appBarButton_Click;

        //    // Create a new menu item with the localized string from AppResources.
        //    //ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    //ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}

        public static void console(string message)
        {
            //System.Diagnostics.Debug.WriteLine(message);
        }

        public static void ShowToast(string message)
        { 
            // create a Coding4Fun toolkit ToastPrompt
            ToastPrompt toast = new ToastPrompt();
            toast.Title = "Paperback";
            toast.Message = message;
            toast.VerticalAlignment = VerticalAlignment.Top;
            toast.TextOrientation = System.Windows.Controls.Orientation.Vertical;
            toast.ImageSource = new BitmapImage(new Uri("/Assets/Images/pb_toast.png", UriKind.Relative));
            toast.Show();
        }

        public static void ShowMessageBox(bool noMsg, string type)
        {
            string message = "";
            if(type.Equals("NetError"))
            {
                message = "Unable to connect.";
            }
            MessageBox.Show(message, "Paperback", MessageBoxButton.OK);
        }

        public static void ShowMessageBox(string message)
        {
            MessageBox.Show(message, "Paperback", MessageBoxButton.OK);
        }
    }
}