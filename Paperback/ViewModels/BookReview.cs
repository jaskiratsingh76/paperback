﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Paperback.ViewModels
{
    public class BookReview
    {
        public List<Review> Reviews { get; set; }

        // constructor
        public BookReview()
        {
            Reviews = new List<Review>();
        }
        
        public static async Task<BookReview> GetBookReviews(string bookTitle, string bookAuthor, int startIndex)
        {
            // check valid book title
            if (bookTitle == null)
                return null;

            // create an http client
            HttpClient httpClient = new HttpClient();

            // sample request url
            //http://gr-reviews.appspot.com/reviews.json?title=Kafka%20on%20the%20Shore&author=Haruki%20Murakami

            string requestUrl = "http://gr-reviews.appspot.com/reviews.json" +
                "?title=" + bookTitle;

            if (bookAuthor != null)
            {
                string authorSlug = "&author=" + bookAuthor;
                requestUrl += authorSlug;
            }

            // send request and record result
            string reviewDataJSON = "";
            try
            {
                reviewDataJSON = await httpClient.GetStringAsync(requestUrl);
            }
            catch (System.Net.Http.HttpRequestException)
            {
                // no reviews found
                // show toast
                MainPage.ShowToast("unable to connect");
                return null;
            }

            // deserialise JSON
            GRReviews reviewData;
            try
            {
                // check null reviews
                reviewData = JsonConvert.DeserializeObject<GRReviews>(reviewDataJSON);
            }
            catch (Exception)
            {
                // no reviews found
                // show message
                MainPage.ShowToast("no reviews found");
                return null;
            }

            // extract list of books
            return ExtractData(reviewData);
        }

        private static BookReview ExtractData(GRReviews reviewData)
        {
            // check null reviews
            if (reviewData.count < 1)
            {
                return null;
            }

            // create empty BookReview
            BookReview ReviewsOfBook = new BookReview();
            int reviewID = 0;

            // begin data extraction
            foreach (Rev rev in reviewData.reviews)
            {
                // create empty review
                Review review = new Review();

                // extract info
                review.ID = reviewID;
                review.Author = rev.author;
                review.Link = rev.link;
                review.Snippet = rev.review;

                // add review to BookReview object
                ReviewsOfBook.Reviews.Add(review);
                // increment review ID
                reviewID += 1;
            }
            // return the list of reviews
            return ReviewsOfBook;
        }
    }
}
