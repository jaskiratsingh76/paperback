﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paperback.ViewModels
{
    public class GoogleDataStructure
    {
        public int totalItems { get; set; }
        public List<Item> items { get; set; }
    }

    public class Item
    {
        public VolumeInfo volumeInfo { get; set; }
    }

    public class VolumeInfo
    {
        public string title { get; set; }
        public List<string> authors { get; set; }
        public string publisher { get; set; }
        public string publishedDate { get; set; }
        public string description { get; set; }
        public List<IndustryIdentifier> industryIdentifiers { get; set; } // contain the ISBN or other identifiers
        public int pageCount { get; set; }
        public double averageRating { get; set; }
        public int ratingsCount { get; set; } // ignored
        public ImageLinks imageLinks { get; set; }
        public string subtitle { get; set; } // ignored
    }

    public class IndustryIdentifier
    {
        public string type { get; set; }
        public string identifier { get; set; }
    }

    public class ImageLinks
    {
        public string thumbnail { get; set; }
    }
}
