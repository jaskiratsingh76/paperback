﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paperback.ViewModels
{
    public class GRReviews
    {
        public int count { get; set; }
        public List<Rev> reviews { get; set; }
    }

    public class Rev
    {
        public string review { get; set; }
        public string link { get; set; }
        public string author { get; set; }
    }
}
