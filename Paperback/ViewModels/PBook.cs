﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paperback.ViewModels
{
    public class PBook
    {
        public GoogleBook GBook { get; set; }
        public BookReview ReviewOfBook { get; set; }

        public bool IsDataLoaded { get; set; }

        public void LoadData()
        {
            // Load data here

            IsDataLoaded = true;
        }

        public PBook()
        { }

        public PBook(GoogleBook book, BookReview bookReview)
        {
            this.GBook = book;
            this.ReviewOfBook = bookReview;
        }
    }
}
