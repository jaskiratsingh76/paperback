﻿using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Paperback.ViewModels
{
    public class GoogleBook
    {
        // structure for a book returned by Google API

        // industry identifier, ideally ISBN
        // but different kind where ISBN not available
        public int Index { get; set; }
        public string IdentifierType { get; set; }
        public string Identifier { get; set; }
        public string DefaultCoverImage { get; set; }
        public string YearAndPages { get; set; }
        public string Year { get; set; }

        // titles, author(s) and description
        public string Title { get; set; }
        public string ShortTitle { get; set; }
        public string Authors { get; set; }
        public List<String> AuthorsList { get; set; }
        public string Description { get; set; }
        public string CoverImage { get; set; }

        // fallback data for GoodReads API
        public double Rating { get; set; }
        public int PageCount { get; set; }
        public string Publisher { get; set; }
        public string PublishedDate { get; set; }

        // this class acts as a wrapper for the call made to the Google Books API
        // also, it's used to represent one book fetched from the Google Books API

        // return a list of books returned by the API for a search query
        public static async Task<List<GoogleBook>> GetGoogleBook(string query, int startIndex)
        {
            // check query string
            if (query == null)
                return null;

            // create an http client
            HttpClient httpClient = new HttpClient();
            string googleDataJSON = "";

            // sample request url
            // https://www.googleapis.com/books/v1/volumes?q=norwegian%20wood%20murakami
            // https://www.googleapis.com/books/v1/volumes?q=norwegian%20wood%20murakami&maxResults=10&startIndex=2

            string baseUrl = "https://www.googleapis.com/books/v1/volumes" +
                "?q={0}" + 
                "&startIndex={1}" + 
                "&maxResults=10";

            var requestUrl = string.Format(baseUrl, query, startIndex);

            // send request and record result
            try
            {
                googleDataJSON = await httpClient.GetStringAsync(requestUrl);
            }
            catch (System.Net.Http.HttpRequestException)
            {
                // internet not working
                // show error toast
                MainPage.ShowToast("unable to connect");
                return null;
            }

            // deserialise JSON
            GoogleDataStructure googleData;
            try
            {
                googleData = JsonConvert.DeserializeObject<GoogleDataStructure>(googleDataJSON);
            }
            catch (Exception)
            {
                // show error toast
                MainPage.ShowToast("unable to connect");
                return null;
            }

            // extract list of books
            return ExtractData(googleData, startIndex);
        }

        private static List<GoogleBook> ExtractData(GoogleDataStructure googleData, int startIndex)
        {

            // check if any book came back
            if (googleData.totalItems < 1)
                return null;

            // init index
            int index = startIndex;

            // empty list of books
            List<GoogleBook> gBooks = new List<GoogleBook>();

            // begin data extraction
            foreach (Item rawBook in googleData.items)
            {
                // create empty book
                GoogleBook gBook = new GoogleBook();

                // extracting data
                gBook.Index = index;
                try { gBook.Title = rawBook.volumeInfo.title; }
                catch (Exception) { gBook.Title = "Unknown title"; }

                try { gBook.Rating = rawBook.volumeInfo.averageRating; }
                catch (Exception) { gBook.Rating = 2.5; }

                try 
                { gBook.Description = rawBook.volumeInfo.description; }
                catch (Exception) { gBook.Description = null; }

                try
                { 
                    gBook.PageCount = rawBook.volumeInfo.pageCount;
                    if (gBook.PageCount == null)
                    {
                        gBook.PageCount = 0;
                    }
                }
                catch (Exception) { gBook.PageCount = 0; }

                try 
                { 
                    gBook.PublishedDate = rawBook.volumeInfo.publishedDate;
                    if (gBook.PublishedDate == null)
                    {
                        gBook.PublishedDate = "";
                    }
                }
                catch (Exception) { gBook.PublishedDate = ""; }

                try
                { 
                    gBook.Publisher = rawBook.volumeInfo.publisher;
                    if (gBook.Publisher == null)
                    {
                        gBook.Publisher = "Unknown publisher";
                    }
                }
                catch (Exception) { gBook.Publisher = "Unknown publisher"; }

                try 
                { 
                    gBook.CoverImage = rawBook.volumeInfo.imageLinks.thumbnail;
                    if (gBook.CoverImage == null)
                    {
                        gBook.CoverImage = gBook.DefaultCoverImage;
                    }
                    // remove image curl
                    gBook.CoverImage = RemoveCurl(gBook.CoverImage);
                }
                catch (Exception) { gBook.CoverImage = gBook.DefaultCoverImage; }

                // short title
                if (rawBook.volumeInfo.title.Length > 45)
                {
                    gBook.ShortTitle = rawBook.volumeInfo.title.Substring(0, 45);
                }
                else
                {
                    gBook.ShortTitle = rawBook.volumeInfo.title;
                }
                gBook.ShortTitle = gBook.ShortTitle.ToUpper();

                try
                {
                    // store authors list
                    foreach (String author in rawBook.volumeInfo.authors)
                    {
                        gBook.AuthorsList.Add(author);
                    }

                    // string of authors, comma separated
                    gBook.Authors = "";
                    gBook.Authors += rawBook.volumeInfo.authors.ElementAt(0);
                    rawBook.volumeInfo.authors.RemoveAt(0);

                    while (rawBook.volumeInfo.authors.Count >= 1)
                    {
                        gBook.Authors += ", ";
                        gBook.Authors += rawBook.volumeInfo.authors.ElementAt(0);
                        rawBook.volumeInfo.authors.RemoveAt(0);
                    }
                }
                catch (Exception)
                {
                    gBook.AuthorsList = null;
                    gBook.Authors = "Unknown author";
                }


                // format published year and pages together
                gBook.Year = GetYear(gBook.PublishedDate);
                gBook.YearAndPages = gBook.Year + " | " + gBook.PageCount + " pages";

                if ((gBook.PublishedDate == "") && (gBook.PageCount != 0))
                {
                    gBook.YearAndPages = gBook.PageCount.ToString();
                    gBook.YearAndPages += " pages";
                }
                else if ((gBook.PublishedDate != "") && (gBook.PageCount == 0))
                {
                    gBook.YearAndPages = gBook.Year;
                }
                else if( (gBook.PublishedDate == "") && (gBook.PageCount == 0))
                {
                    gBook.YearAndPages = "";
                }

                try
                {
                    // extract industry identifier
                    foreach (IndustryIdentifier id in rawBook.volumeInfo.industryIdentifiers)
                    {
                        if (id.type == "ISBN_10")
                        {
                            gBook.IdentifierType = "ISBN";
                            gBook.Identifier = GetFormattedIndentifier(10, id.identifier);
                        }
                        else if (id.type == "ISBN_13")
                        {
                            gBook.IdentifierType = "ISBN";
                            gBook.Identifier = GetFormattedIndentifier(13, id.identifier);
                            break;
                        }
                        else if (id.type == "OTHER")
                        {
                            gBook.IdentifierType = GetOtherIdentifierType(id.identifier);
                            gBook.Identifier = GetOtherIdentifier(id.identifier);
                        }
                    }
                }
                catch (Exception)
                {
                    gBook.IdentifierType = "";
                    gBook.Identifier = "";
                }

                // add book to the list of books
                gBooks.Add(gBook);
                index += 1;
            }
         
            // return the list of books
            return gBooks;
        }

        private static string RemoveCurl(string url)
        {
            // remove edge=curl from the url
            // http://bks9.books.google.co.in/books?id=wTVDZcB_FnAC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api
            return url.Replace("&edge=curl", "");
        }

        private static string GetOtherIdentifierType(string identifier)
        {
            // split the identifier and return the type of identifier
            return identifier.Split(':')[0];
        }

        private static string GetOtherIdentifier(string identifier)
        {
            return identifier.Split(':')[1];
        }

        private static string GetFormattedIndentifier(int identifier_type, string identifier)
        {
            string formatted_isbn;
            // inserts dashes for ISBN 10 and 13
            if ( (identifier_type == 13) && (identifier.Length == 13) )
            {
                formatted_isbn = identifier.Substring(0, 3);
                formatted_isbn += "-";
                formatted_isbn += identifier.Substring(3, 1);
                formatted_isbn += "-";
                formatted_isbn += identifier.Substring(4, 4);
                formatted_isbn += "-";
                formatted_isbn += identifier.Substring(8, 4);
                formatted_isbn += "-";
                formatted_isbn += identifier.Substring(12, 1);
                
                return formatted_isbn;
            }
            else if ( (identifier_type == 10) && (identifier.Length == 10) )
            {
                formatted_isbn = identifier.Substring(0, 1);
                formatted_isbn += "-";
                formatted_isbn += identifier.Substring(1, 4);
                formatted_isbn += "-";
                formatted_isbn += identifier.Substring(5, 4);
                formatted_isbn += "-";
                formatted_isbn += identifier.Substring(9, 1);
                
                return formatted_isbn;
            }
            else
                return identifier;
        }

        private static string GetYear(string date)
        {
            // date format: 2011-10-10, return 2011
            return date.Split('-')[0];
        }

        // consdtructor
        public GoogleBook()
        {
            AuthorsList = new List<String>();
            this.DefaultCoverImage = "/Assets/Images/pb_default_cover.png";
        }

        public GoogleBook(String title, String authors, String coverImage)
        {
            AuthorsList = new List<String>();
            this.Title = title;
            this.Authors = authors;
            this.CoverImage = coverImage;
            this.DefaultCoverImage = "/Assets/Images/pb_default_cover.png";
        }
    }
}
