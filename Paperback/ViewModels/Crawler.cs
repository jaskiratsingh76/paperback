﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Paperback.ViewModels
{
    public class Crawler
    {
        /**
         * wrapper class for performing search on the Google Books API 
         * fetching the reviews, and combining them to form a Book
         */

        // fetch information from the API's
        public static async Task<List<GoogleBook>> GetBooks(string queryString, int startIndex)
        {
            try
            {
                return await GoogleBook.GetGoogleBook(queryString, startIndex);
            }
            catch (Exception ex)
            {
                MainPage.console(ex.StackTrace + ", MSG: " + ex.Message);
                return null;
            }
        }

        public static async Task<BookReview> GetBookReviews(string bookTitle, string bookAuthor, int startIndex)
        {
            try 
            {
                return await BookReview.GetBookReviews(bookTitle, bookAuthor, startIndex);
            }
            catch(Exception ex)
            {
                MainPage.console(ex.StackTrace + ", MSG: " + ex.Message);
                return null;
            }
        }
    }
}
