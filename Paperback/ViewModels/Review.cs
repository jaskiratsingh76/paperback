﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paperback.ViewModels
{
    public class Review
    {
        public int ID { get; set; }
        public string Author { get; set; }
        public string Link { get; set; }
        public string Snippet { get; set; }
    }
}
