﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Paperback.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AppResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AppResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Paperback.Resources.AppResources", typeof(AppResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to about.
        /// </summary>
        public static string AboutPivot {
            get {
                return ResourceManager.GetString("AboutPivot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to about.
        /// </summary>
        public static string AppBarButtonText {
            get {
                return ResourceManager.GetString("AppBarButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Menu Item.
        /// </summary>
        public static string AppBarMenuItemText {
            get {
                return ResourceManager.GetString("AppBarMenuItemText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PAPERBACK.
        /// </summary>
        public static string ApplicationTitle {
            get {
                return ResourceManager.GetString("ApplicationTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to authors.
        /// </summary>
        public static string AuthorsPivot {
            get {
                return ResourceManager.GetString("AuthorsPivot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to description.
        /// </summary>
        public static string BDDescriptionPivot {
            get {
                return ResourceManager.GetString("BDDescriptionPivot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to more.
        /// </summary>
        public static string BDMorePivot {
            get {
                return ResourceManager.GetString("BDMorePivot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to reviews.
        /// </summary>
        public static string BDReviewsPivot {
            get {
                return ResourceManager.GetString("BDReviewsPivot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to title.
        /// </summary>
        public static string BDTitlePivot {
            get {
                return ResourceManager.GetString("BDTitlePivot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to favorites.
        /// </summary>
        public static string FavoritesPivot {
            get {
                return ResourceManager.GetString("FavoritesPivot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to help.
        /// </summary>
        public static string HelpPivot {
            get {
                return ResourceManager.GetString("HelpPivot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to history.
        /// </summary>
        public static string HistoryPivot {
            get {
                return ResourceManager.GetString("HistoryPivot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to home.
        /// </summary>
        public static string HomePivot {
            get {
                return ResourceManager.GetString("HomePivot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to more.
        /// </summary>
        public static string MorePivot {
            get {
                return ResourceManager.GetString("MorePivot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to LeftToRight.
        /// </summary>
        public static string ResourceFlowDirection {
            get {
                return ResourceManager.GetString("ResourceFlowDirection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to en-US.
        /// </summary>
        public static string ResourceLanguage {
            get {
                return ResourceManager.GetString("ResourceLanguage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to reviews.
        /// </summary>
        public static string ReviewsPivot {
            get {
                return ResourceManager.GetString("ReviewsPivot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sample Runtime Property Value.
        /// </summary>
        public static string SampleProperty {
            get {
                return ResourceManager.GetString("SampleProperty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to search.
        /// </summary>
        public static string SearchPage {
            get {
                return ResourceManager.GetString("SearchPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to results.
        /// </summary>
        public static string SearchResultsPage {
            get {
                return ResourceManager.GetString("SearchResultsPage", resourceCulture);
            }
        }
    }
}
